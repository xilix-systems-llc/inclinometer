package main

import (
	"fmt"
	"os"
	"os/signal"

	"github.com/xilixsys/hid"

	"time"
)

func main() {
	fmt.Println("test")
	devices := hid.FindDevices(0x24cc, 0x003a)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	//devices := hid.Devices()
	// devices := hid.FindDevices(0x04f3, 0x015b)
	var devicePaths []string
	for device := range devices {
		devicePaths = append(devicePaths, device.Path)
		fmt.Println(device.Path)
	}
	goodPath := devicePaths[0]
	goodHid, err := hid.ByPath(goodPath)
	if err != nil {
		fmt.Println(err)
	}
	openedHid, _ := goodHid.Open()
	sendBytes := make([]byte, 65)
	sendBytes[1] = 0x06
	sendBytes[2] = 0x01
	// sendBytes[3] = 0x02
	err = openedHid.Write(sendBytes)
	if err != nil {
		fmt.Println(err)
	}
	var prevTime time.Time
	var prevAngle float64
	var maxAngle float64
	var minAngle float64
	var maxSpeed float64
	var minSpeed float64
	go func() {
		for {
			//sendData := []byte {0x06, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00 }
			//openedHid.Write(sendData)
			returnedData, err := openedHid.Read(64)
			if err != nil {
				fmt.Println(err)
			}

			// fmt.Printf("data %d, %d, %d, %d", returnedData[1], returnedData[2], returnedData[3], returnedData[8])
			if int(returnedData[1]) == 1 && int(returnedData[3]) == 1 {
				//if int(returnedData[1]) == 1 && int(returnedData[3]) == 2 && int(returnedData[8]) == 0x0b{
				/*   for _, data := range returnedData {
				    fmt.Println(data)
				}
				for _, path := range devicePaths {
				    fmt.Println(path)
				}
				*/

				byte6Val := int(returnedData[6]) << 16
				byte5Val := int(returnedData[5]) << 8
				totalByteVal := byte6Val + byte5Val + int(returnedData[4])
				subtractor := 1800000.0
				dividor := 10000.0

				if int(returnedData[3]) == 1 {
					subtractor = 180000.0
					dividor = 1000.0
				}

				subtract18K := float64(totalByteVal) - subtractor
				divItUp := float64(subtract18K) / dividor
				if divItUp > maxAngle {
					maxAngle = divItUp
				}
				if divItUp < minAngle {
					minAngle = divItUp
				}

				currentTime := time.Now()
				if prevTime.IsZero() {
					prevTime = currentTime
					prevAngle = 0
				} else {

					timeDiff := currentTime.Sub(prevTime)
					prevTime = currentTime

					angleChange := prevAngle - divItUp
					prevAngle = divItUp

					velocity := angleChange / timeDiff.Seconds()

					if velocity > maxSpeed {
						maxSpeed = velocity
					}
					if velocity < minSpeed {
						minSpeed = velocity
					}

					if minAngle < -10 || maxAngle > 10 || minSpeed < -20 || maxSpeed > 20 {
						minAngle = 0
						maxAngle = 0
						minSpeed = 0
						maxSpeed = 0
						divItUp = 0
						angleChange = 0
						velocity = 0
					}

					fmt.Printf("\rV: %+02.2f A %+01.2f A%+01.1f T %+01.3f MinA %+01.2f MaxA %+01.2f MinV %+01.2f MaxV %+01.2f",
						velocity,
						angleChange,
						divItUp,
						timeDiff.Seconds(),
						minAngle,
						maxAngle,
						minSpeed,
						maxSpeed)
					// fmt.Println(returnedData[1], divItUp, returnedData[7])
					time.Sleep(time.Millisecond * 750)
				}

			}
		}
	}()

	<-c
	openedHid.Close()
	// hid.ByPath()
}
